## Section 1 - API
-------
#### 1 - HTTP Methods
>**GET** - Retrieve<br>
>**POST** - Create, update, and retrieve with sensitive metadata<br>
>**PUT** - Create and update<br>
>**UPDATE**(**PATCH**?) - Update<br>

#### Examples: List of APIs for typical ecommerce app
##### Retrieving a product/list of products
GET /product/<optional_id_of_product>

##### Creating a product
POST /product/ - with product payload
PUT /product/<new_id> - with product payload

##### Updating a product
POST /product/<id> - with update payload
PUT /product/<id> - with update payload
PATCH /product/<id> - with (partial) update payload

#### 2 - API Authentication
Token-based authentication + HTTP Basic/Digest authentication. 
1. User send digest/plaintext(over HTTPS) of username + password to the server.
2. If authenticated, server signs a token with server secret key.
3. User gets the token, along with token expiry timestamp, optionally with a refresh token.
4. Subsequent API calls can be authenticated with token, or until it expires.

#### 3 - Return Format
JSON. Lightweight versus XML, and native in browsers. Easy to implement and suitable for most CRUD operations.

---------
## Section 2 - Checkout Program
---------
#### Run
```bash
# Clone the code
git clone https://gitlab.com/shawngoh87/digix-lightning.git

# Start checkout system PoC in browser
php -S 0.0.0.0:9000
```
#### View Result
[Your localhost:9000](http://localhost:9000)

#### Source code
Look for ```index.php``` in this repo.