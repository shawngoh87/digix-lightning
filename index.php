<?php

/**
 * Rules:
 * - atv, 3 for 2
 * - ipd, 499.99 for quantity > 4
 * - vga free per every mbp
 */
class Checkout{

    private $DB = [
        'ipd' => ['NAME' => 'Super iPad', 'Price' => 549.99],
        'mbp' => ['NAME' => 'MacBook Pro', 'Price' => 1399.99],
        'atv' => ['NAME' => 'Apple TV', 'Price' => 109.50],
        'vga' => ['NAME' => 'VGA Adapter', 'Price' => 30.00]
    ];
    private $total = 0;
    private $basket = ['ipd' => 0, 'mbp' => 0, 'atv' => 0, 'vga' => 0];

    public function scan(...$items){
        foreach($items as $item){
            $this->total += $this->DB[$item]['Price'];
            $this->basket[$item] += 1;
        }
        $this->__reprocessPrice();
        return $this;
    }

    public function total(){
        $temp = $this->total;
        $this->__resetInternal();
        return $temp;
    }

    private function __reprocessPrice(){
        $this->total -= floor($this->basket['atv'] / 3) * $this->DB['atv']['Price'];
        if ($this->basket['ipd'] > 4) {
            $this->total -= $this->basket['ipd'] * ($this->DB['ipd']['Price'] - 499.99);
        }
        $this->total -= $this->basket['mbp'] * $this->DB['vga']['Price'];
    }

    private function __resetInternal(){
        $this->total = 0;
        $this->basket = ['ipd' => 0, 'mbp' => 0, 'atv' => 0, 'vga' => 0];
    }
}


$co = new Checkout();
$itemSet_1 = ['atv', 'atv', 'atv', 'vga'];
$itemSet_2 = ['atv', 'ipd', 'ipd', 'atv', 'ipd', 'ipd', 'ipd'];
$itemSet_3 = ['mbp', 'vga', 'ipd'];
$total_1 = $co->scan(...$itemSet_1)->total();
$total_2 = $co->scan(...$itemSet_2)->total();
$total_3 = $co->scan(...$itemSet_3)->total();

print('SKUs Scanned: '.implode(',', $itemSet_1));
print('<br>');
print('Total expected : $'.$total_1);
print('<br>');
print('SKUs Scanned: '.implode(',', $itemSet_2));
print('<br>');
print('Total expected : $'.$total_2);
print('<br>');
print('SKUs Scanned: '.implode(',', $itemSet_3));
print('<br>');
print('Total expected : $'.$total_3);
print('<br>');

?>